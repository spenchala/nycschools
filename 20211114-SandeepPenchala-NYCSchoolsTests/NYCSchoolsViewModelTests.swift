//
//  NYCSchoolsViewModelTests.swift
//  20211114-SandeepPenchala-NYCSchoolsTests
//
//  Created by Sandeep Penchala on 11/14/21.
//

import XCTest
@testable import _0211114_SandeepPenchala_NYCSchools

class NYCSchoolsViewModelTests: XCTestCase {
    var nycHighSchoolsViewModel: NYCHighSchoolsViewModel!
    override func setUp() {
        super.setUp()
        nycHighSchoolsViewModel = NYCHighSchoolsViewModel(MockNYCHighSchoolsService())
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNYCSchoolsViewModel() {
        nycHighSchoolsViewModel.onNYCSchoolsRecieved = { result in
            switch result {
            case .success:
                XCTAssertFalse(self.nycHighSchoolsViewModel.nycHighSchools.isEmpty)
                XCTAssertEqual(self.nycHighSchoolsViewModel.nycHighSchools.count, 440)
                XCTAssertEqual(self.nycHighSchoolsViewModel.title, "NYC Schools")
            case .failure:
                XCTAssertTrue(self.nycHighSchoolsViewModel.nycHighSchools.isEmpty)
            }
            
        }
        nycHighSchoolsViewModel.fetchSchools()
    }
}

class MockNYCSchoolsComponents: APIRequest {
    var baseURL: URL? {
        Bundle(for: type(of: self)).url(forResource: "NYCSchools", withExtension: "json")
    }
    
    var path: String {
        ""
    }
}

class MockNYCHighSchoolsService: NYCSchoolsAPI {
    func fetchNYCSchools(_ completion: @escaping APIResult<[NYCHighSchool], NYCAPIError>) {
        do {
            let request = MockNYCSchoolsComponents().request()
            let data = try Data(contentsOf: (request?.url)!)
            let model = try JSONDecoder().decode([NYCHighSchool].self, from: data)
            onMain { completion(.success(model)) }
        } catch {
            onMain { completion(.failure(.decodingError)) }
        }
    }
}
