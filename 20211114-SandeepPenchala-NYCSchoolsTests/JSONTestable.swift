//
//  JSONTestable.swift
//  20211114-SandeepPenchala-NYCSchoolsTests
//
//  Created by Sandeep Penchala on 11/14/21.
//

import Foundation
protocol JSONTestable {
    init?(_ json: String)
    func json() -> String?
}

extension JSONTestable where Self: Codable {
    init?(_ json: String) {
        guard
            let data = json.data(using: .utf8),
            let decoded = try? JSONDecoder().decode(Self.self, from: data)
            else { return nil }
        self = decoded
    }

    init?(_ url: URL?) {
        guard
            let url = url,
            let data = try? Data(contentsOf: url),
            let decoded = try? JSONDecoder().decode(Self.self, from: data)
            else { return nil }
        self = decoded
    }
    
    func json() -> String? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

