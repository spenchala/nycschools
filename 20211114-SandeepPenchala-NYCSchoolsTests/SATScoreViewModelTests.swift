//
//  SATScoreViewModelTests.swift
//  20211114-SandeepPenchala-NYCSchoolsTests
//
//  Created by Sandeep Penchala on 11/14/21.
//

import XCTest
import CoreLocation

@testable import _0211114_SandeepPenchala_NYCSchools

extension NYCHighSchool: JSONTestable { }

class SATScoreViewModelTests: XCTestCase {

    var satScoresViewModel: SATScoresViewModel!
    lazy var nycSchoolModel: NYCHighSchool = {
        NYCHighSchool(Bundle(for: type(of: self)).url(forResource: "NYCSchool", withExtension: "json"))!
    }()

    override func setUp() {
        super.setUp()
        satScoresViewModel = SATScoresViewModel(nycSchoolModel.dbn ?? "", MockSATScoresService(), nycSchoolModel)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNYCSchoolsViewModel() {
        satScoresViewModel.onSATScoreRecieved = { result in
            switch result {
            case .success:
                XCTAssertFalse(self.satScoresViewModel.satScores.isEmpty)
                XCTAssertEqual(self.satScoresViewModel.clLocationDistance, 400)
                XCTAssertEqual(self.satScoresViewModel.latitude, Double(self.nycSchoolModel.latitude ?? "") ?? 0)
                XCTAssertEqual(self.satScoresViewModel.longitude, Double(self.nycSchoolModel.longitude ?? "") ?? 0)
                XCTAssertEqual(self.satScoresViewModel.invalidData, "No SAT Information")
                XCTAssertEqual(self.satScoresViewModel.title, "SAT Score")
            case .failure:
                XCTAssertTrue(self.satScoresViewModel.satScores.isEmpty)
            }

        }
        satScoresViewModel.fetchSatScores()
    }
}
class MockSATScoresComponents: APIRequest {
    var baseURL: URL? {
        Bundle(for: type(of: self)).url(forResource: "SATScores", withExtension: "json")
    }
    
    var path: String {
        ""
    }
}

class MockSATScoresService: SATScoresAPI {
    func fetchSATScores(_ dbn: String, _ completion: @escaping APIResult<[SATScore], NYCAPIError>) {
        do {
            let request = MockSATScoresComponents().request()
            let data = try Data(contentsOf: (request?.url)!)
            let model = try JSONDecoder().decode([SATScore].self, from: data)
            onMain { completion(.success(model)) }
        } catch {
            onMain { completion(.failure(.decodingError)) }
        }
    }
}
