//
//  NYCHighSchoolViewCell.swift
//  20211114-SandeepPenchala-NYCSchools
//
//  Created by Sandeep Penchala on 11/14/21.
//

import Foundation
import UIKit

final class NYCHighSchoolViewCell: UITableViewCell {
    static let reuseIdentifier = String(describing: NYCHighSchoolViewCell.self)
    
    private lazy var schoolNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 17)
        return label
    }()
    
    private lazy var locationLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        setupSubViews()
    }
    
    override func prepareForReuse() {
        schoolNameLabel.text = nil
    }
    
    func setSchool(_ nycHighSchool: NYCHighSchool) {
        schoolNameLabel.text = nycHighSchool.schoolName
        var locationText = ""
        if let city = nycHighSchool.city {
            locationText = "\(locationText)\(city)"
        }
        if let state = nycHighSchool.stateCode {
            locationText = "\(locationText) \(state)"
        }
        locationLabel.text = locationText.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}

private extension NYCHighSchoolViewCell {
    func setupSubViews() {
        accessoryType = .disclosureIndicator
        addSubview(schoolNameLabel, translatesAutoresizingMaskIntoConstraints: false)
        NSLayoutConstraint.activate([
            schoolNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            schoolNameLabel.trailingAnchor.constraint(equalTo: accessoryView?.trailingAnchor ?? trailingAnchor, constant: -30),
            schoolNameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
        ])
        
        addSubview(locationLabel, translatesAutoresizingMaskIntoConstraints: false)
        NSLayoutConstraint.activate([
            locationLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            locationLabel.trailingAnchor.constraint(equalTo: accessoryView?.trailingAnchor ?? trailingAnchor, constant: -30),
            locationLabel.topAnchor.constraint(equalTo: schoolNameLabel.bottomAnchor, constant: 5),
        ])
    }
}
