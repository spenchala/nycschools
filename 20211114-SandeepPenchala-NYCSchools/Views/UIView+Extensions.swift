//
//  UIView+Extensions.swift
//  20211114-SandeepPenchala-NYCSchools
//
//  Created by Sandeep Penchala on 11/14/21.
//

import UIKit

extension UIView {
    func addSubview(_ subview: UIView, translatesAutoresizingMaskIntoConstraints: Bool) {
        subview.translatesAutoresizingMaskIntoConstraints = translatesAutoresizingMaskIntoConstraints
        addSubview(subview)
    }
}
