//
//  AlertPresenter.swift
//  20211114-SandeepPenchala-NYCSchools
//
//  Created by Sandeep Penchala on 11/14/21.
//

import Foundation
import UIKit

protocol AlertPresenterProtocol {
    func present(from: UIViewController, title: String, message: String, dismissButtonTitle: String, buttonActionHandler: @escaping (UIAlertAction) -> Void)
}

final class AlertPresenter: AlertPresenterProtocol {
    func present(from: UIViewController, title: String, message: String, dismissButtonTitle: String, buttonActionHandler: @escaping (UIAlertAction) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: dismissButtonTitle, style: .default, handler: buttonActionHandler)
        alertController.addAction(alertAction)
        from.present(alertController, animated: true, completion: nil)
    }
}
