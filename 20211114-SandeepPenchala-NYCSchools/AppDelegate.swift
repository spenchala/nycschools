//
//  AppDelegate.swift
//  20211114-SandeepPenchala-NYCSchools
//
//  Created by Sandeep Penchala on 11/14/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: NYCHighSchoolsViewController(NYCHighSchoolsViewModel(NYCSchoolsAPIManager())))
        window?.makeKeyAndVisible()
        return true
    }
}

