//
//  SATScoresViewModel.swift
//  20211114-SandeepPenchala-NYCSchools
//
//  Created by Sandeep Penchala on 11/14/21.
//

import Foundation
import CoreLocation
import MapKit

final class SATScoresViewModel {

    private var apiManager: SATScoresAPI
    private(set) var satScores: [SATScore] = []
    private var nycHighSchool: NYCHighSchool
    private var dbn: String

    var onSATScoreRecieved: ((Result<Void, NYCAPIError>) -> Void)?
    
    var title: String {
        "SAT Score"
    }

    var satScoresText: String {
        guard let satScore = satScores.first else {
            return invalidData
        }
        return
            "Number of SAT Takers: \(satScore.numOfSatTestTakers ?? "")\n"
                .appending("Avg. Math Score: \(satScore.satMathAvgScore ?? "")\n")
                .appending("Avg. Reading Score: \(satScore.satCriticalReadingAvgScore ?? "")\n")
                .appending("Avg. Writing Score: \(satScore.satWritingAvgScore ?? "")\n")
                .appending("Website: \(nycHighSchool.website ?? "")\n")
                .trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var schoolName: String {
        "School Name: \(nycHighSchool.schoolName ?? "")"
    }
    
    var invalidData: String {
        "No SAT Information"
    }

    var latitude: Double {
        Double(nycHighSchool.latitude ?? "") ?? 0.0
    }
    
    var longitude: Double {
        Double(nycHighSchool.longitude ?? "") ?? 0.0
    }
    
    var clLocation: CLLocation {
       CLLocation(latitude: latitude, longitude: longitude)
    }

    var clLocationDistance: CLLocationDistance {
        400
    }
    
    var mapCamera: MKMapCamera {
        let mapCamera = MKMapCamera()
        mapCamera.centerCoordinate = CLLocationCoordinate2DMake(latitude, longitude)
        mapCamera.pitch = 45
        mapCamera.altitude = clLocationDistance
        mapCamera.heading = 45
        return mapCamera
    }

    init(
        _ dbn: String,
        _ apiManager: SATScoresAPI,
        _ nycHighSchool: NYCHighSchool
    ) {
        self.dbn = dbn
        self.apiManager = apiManager
        self.nycHighSchool = nycHighSchool
    }
    
    func fetchSatScores() {
        apiManager.fetchSATScores(dbn) { [weak self] result in
            switch result {
            case let .success(satScore):
                self?.satScores = satScore
                self?.onSATScoreRecieved?(.success(()))
            case let .failure(error):
                self?.onSATScoreRecieved?(.failure(error))
            }
        }
    }
}
