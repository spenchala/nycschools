//
//  NYCHighSchoolsViewModel.swift
//  20211114-SandeepPenchala-NYCSchools
//
//  Created by Sandeep Penchala on 11/14/21.
//

import Foundation

final class NYCHighSchoolsViewModel {

    private var apiManager: NYCSchoolsAPI
    private(set) var nycHighSchools: [NYCHighSchool] = []

    var onNYCSchoolsRecieved: ((Result<Void, NYCAPIError>) -> Void)?
    
    var title: String {
        "NYC Schools"
    }

    init(_ apiManager: NYCSchoolsAPI) {
        self.apiManager = apiManager
    }
    
    func fetchSchools() {
        apiManager.fetchNYCSchools { [weak self] result in
            switch result {
            case let .success(schools):
                self?.nycHighSchools = schools
                self?.onNYCSchoolsRecieved?(.success(()))
            case let .failure(error):
                self?.onNYCSchoolsRecieved?(.failure(error))
            }
        }
    }
}
