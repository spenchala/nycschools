// MARK: - NYCHighSchool
struct NYCHighSchool: Codable {
    let buildingCode : String?
    let bus : String?
    let city : String?
    let dbn : String?
    let faxNumber : String?
    let finalgrades : String?
    let grades2018 : String?
    let latitude : String?
    let location : String?
    let longitude : String?
    let neighborhood : String?
    let phoneNumber : String?
    let primaryAddressLine1 : String?
    let schoolEmail : String?
    let schoolName : String?
    let stateCode : String?
    let subway : String?
    let totalStudents : String?
    let website : String?
    let zip : String?
    
    enum CodingKeys: String, CodingKey {
        case buildingCode = "building_code"
        case bus = "bus"
        case city = "city"
        case dbn = "dbn"
        case faxNumber = "fax_number"
        case finalgrades = "finalgrades"
        case grades2018 = "grades2018"
        case latitude = "latitude"
        case location = "location"
        case longitude = "longitude"
        case neighborhood = "neighborhood"
        case phoneNumber = "phone_number"
        case primaryAddressLine1 = "primary_address_line_1"
        case schoolEmail = "school_email"
        case schoolName = "school_name"
        case stateCode = "state_code"
        case subway = "subway"
        case totalStudents = "total_students"
        case website = "website"
        case zip = "zip"
    }
}
