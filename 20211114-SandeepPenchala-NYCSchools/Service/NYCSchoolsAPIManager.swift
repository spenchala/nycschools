//
//  NYCSchoolsAPIManager.swift
//  20211114-SandeepPenchala-NYCSchools
//
//  Created by Sandeep Penchala on 11/14/21.
//

import Foundation

typealias APIResult<T: Codable, E: Error> = (Result<T, E>) -> Void

enum NYCAPIError: Error {
    case invalidRequest
    case apiFailure
    case dataCorrupted
    case decodingError
}

protocol NYCSchoolsAPI {
    func fetchNYCSchools(_ completion: @escaping APIResult<[NYCHighSchool], NYCAPIError>)
}

protocol SATScoresAPI {
    func fetchSATScores(_ dbn: String, _ completion: @escaping APIResult<[SATScore], NYCAPIError>)
}

final class NYCSchoolsAPIManager {
    private let session: URLSession = .shared
}

extension NYCSchoolsAPIManager: NYCSchoolsAPI {
    func fetchNYCSchools(_ completion: @escaping (Result<[NYCHighSchool], NYCAPIError>) -> Void) {
        guard let request = NYCSchoolsComponents().request() else {
            onMain { completion(.failure(.invalidRequest)) }
            return
        }
        session.dataTask(with: request) { data, response, error in
            if let _ = error {
                print(String(describing: error))
                onMain { completion(.failure(.apiFailure)) }
                return
            }
            
            do {
                guard let data = data else {
                    onMain { completion(.failure(.dataCorrupted)) }
                    return
                }
                let model = try JSONDecoder().decode([NYCHighSchool].self, from: data)
                onMain { completion(.success(model)) }
                return
            } catch let error {
                print(error)
                onMain { completion(.failure(.decodingError)) }
                return
            }
        }.resume()
    }
}


extension NYCSchoolsAPIManager: SATScoresAPI {
    func fetchSATScores(_ dbn: String, _ completion: @escaping APIResult<[SATScore], NYCAPIError>) {
        guard let request = SATScoreComponents(dbn).request() else {
            onMain { completion(.failure(.invalidRequest)) }
            return
        }
        session.dataTask(with: request) { data, response, error in
            if let _ = error {
                print(String(describing: error))
                onMain { completion(.failure(.apiFailure)) }
                return
            }
            
            do {
                guard let data = data else {
                    onMain { completion(.failure(.dataCorrupted)) }
                    return
                }
                let model = try JSONDecoder().decode([SATScore].self, from: data)
                onMain { completion(.success(model)) }
                return
            } catch let error {
                print(error)
                onMain { completion(.failure(.decodingError)) }
                return
            }
        }.resume()
    }
}
