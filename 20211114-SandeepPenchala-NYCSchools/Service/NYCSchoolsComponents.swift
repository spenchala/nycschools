//
//  NYCSchoolsComponents.swift
//  20211114-SandeepPenchala-NYCSchools
//
//  Created by Sandeep Penchala on 11/14/21.
//

import Foundation


struct NYCSchoolsComponents: APIRequest {

    var path: String {
        "resource/s3k6-pzi2.json"
    }
}
