//
//  APIRequest.swift
//  20211114-SandeepPenchala-NYCSchools
//
//  Created by Sandeep Penchala on 11/14/21.
//

import Foundation

func onMain(block: @escaping () -> Void) {
    if Thread.isMainThread {
        block()
        return
    }
    DispatchQueue.main.async {
        block()
    }
}

enum HttpMethod: String {
    case get, post, put, delete
    var value: String {
        return self.rawValue
    }
}

protocol APIRequest {
    var method: HttpMethod { get }
    var baseURL: URL? { get }
    var parameters: [String : String] { get }
    var headers: [String: String] { get }
    var path: String { get }
}

extension APIRequest {
    
    var method: HttpMethod { .get }

    var headers: [String : String] { [:] }

    var parameters: [String : String] { [:] }

    var baseURL: URL? {
        URL(string: "https://data.cityofnewyork.us/")
    }
    
    var path: String { "" }
    
    func request() -> URLRequest? {
        guard let baseURL = baseURL,
              var components = URLComponents(url: baseURL.appendingPathComponent(path),
                                             resolvingAgainstBaseURL: false) else {
            fatalError("Unable to create URL components")
        }

        if !parameters.isEmpty {
            components.queryItems = parameters.map {
                URLQueryItem(name: String($0), value: String($1))
            }
        }

        guard let url = components.url else {
            fatalError("Could not get url")
        }

        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = headers
        request.httpMethod = method.rawValue
        return request
    }
}
