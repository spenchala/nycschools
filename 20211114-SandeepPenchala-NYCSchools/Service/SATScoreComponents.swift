//
//  SATScoreComponents.swift
//  20211114-SandeepPenchala-NYCSchools
//
//  Created by Sandeep Penchala on 11/14/21.
//

import Foundation

struct SATScoreComponents: APIRequest {
    var path: String {
        "resource/f9bf-2cp4.json"
    }

    private var dbn: String

    var parameters: [String : String] {
        ["dbn": dbn]
    }

    init(_ dbn: String) {
        self.dbn = dbn
    }
}
