//
//  ViewController.swift
//  20211114-SandeepPenchala-NYCSchools
//
//  Created by Sandeep Penchala on 11/14/21.
//

import UIKit

final class NYCHighSchoolsViewController: UIViewController {

    private(set) var viewModel: NYCHighSchoolsViewModel
    private let alertPresenter: AlertPresenterProtocol
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(NYCHighSchoolViewCell.self, forCellReuseIdentifier: NYCHighSchoolViewCell.reuseIdentifier)
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return tableView
    }()

    private var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .medium)
        activityIndicator.color = .darkGray
        activityIndicator.startAnimating()
        return activityIndicator
    }()

    init(_ viewModel: NYCHighSchoolsViewModel,
         _ alertPresenter: AlertPresenterProtocol = AlertPresenter()
    ) {
        self.viewModel = viewModel
        self.alertPresenter = alertPresenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubViews()
        viewModel.fetchSchools()
        viewModel.onNYCSchoolsRecieved = { [weak self] result in
            guard let `self` = self else {
                return
            }
            self.activityIndicator.stopAnimating()
            switch result {
            case .success:
                self.tableView.reloadData()
            case let .failure(error):
                self.presentError(error)
            }
        }
    }
}

// MARK: - UITableViewDelegate
extension NYCHighSchoolsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nycHighSchool = viewModel.nycHighSchools[indexPath.row]
        guard
            let dbn = nycHighSchool.dbn else {
            return
        }
        navigationController?.pushViewController(SATScoreViewController(SATScoresViewModel(dbn, NYCSchoolsAPIManager(), nycHighSchool)), animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        80
    }
}

// MARK: - UITableViewDataSource
extension NYCHighSchoolsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.nycHighSchools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: NYCHighSchoolViewCell.reuseIdentifier,
            for: indexPath
          ) as? NYCHighSchoolViewCell else {
            preconditionFailure("Invalid cell type")
        }
        cell.setSchool(viewModel.nycHighSchools[indexPath.row])
        return cell
    }
}

// MARK: - Private Extension
private extension NYCHighSchoolsViewController {
    func presentError(_ error: NYCAPIError) {
        alertPresenter.present(from: self, title: "Error", message: "\(error.localizedDescription)",
                                    dismissButtonTitle: "Retry") { [weak self] _ in
            self?.activityIndicator.startAnimating()
            self?.viewModel.fetchSchools()
        }
    }
    func setupSubViews() {
        title = viewModel.title
        view.backgroundColor = UIColor(named: "viewBackground")
        view.addSubview(tableView, translatesAutoresizingMaskIntoConstraints: false)

        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor),
        ])
        
        view.addSubview(activityIndicator, translatesAutoresizingMaskIntoConstraints: false)
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: view.layoutMarginsGuide.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: view.layoutMarginsGuide.centerYAnchor),
        ])
    }
}

