//
//  SATScoreViewController.swift
//  20211114-SandeepPenchala-NYCSchools
//
//  Created by Sandeep Penchala on 11/14/21.
//

import UIKit
import MapKit

final class SATScoreViewController: UIViewController {

    private(set) var viewModel: SATScoresViewModel
    
    private lazy var mapView: MKMapView = {
        let mapView = MKMapView(frame: .zero)
        mapView.layer.masksToBounds = true
        mapView.layer.cornerRadius = 3.0
        return mapView
    }()

    private lazy var satScoresTextView: UITextView = {
        let textView = UITextView()
        textView.textColor = .darkGray
        textView.font = UIFont.systemFont(ofSize: 18)
        textView.isEditable = false
        textView.dataDetectorTypes = UIDataDetectorTypes.all
        textView.sizeToFit()
        textView.isScrollEnabled = false
        return textView
    }()
    
    private lazy var schoolNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 18)
        return label
    }()
    

    private var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .medium)
        activityIndicator.color = .darkGray
        activityIndicator.startAnimating()
        return activityIndicator
    }()

    init(_ viewModel: SATScoresViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubViews()
        viewModel.fetchSatScores()
        viewModel.onSATScoreRecieved = { [weak self] result in
            guard let `self` = self else {
                return
            }
            self.activityIndicator.stopAnimating()
            switch result {
            case .success:
                self.updateViews()
            case .failure:
                self.schoolNameLabel.text = self.viewModel.invalidData
            }
        }
    }
}

// MARK: - Private Extension
private extension SATScoreViewController {
    func setupSubViews() {
        title = viewModel.title
        view.backgroundColor = UIColor(named: "viewBackground")
        view.addSubview(activityIndicator, translatesAutoresizingMaskIntoConstraints: false)
        NSLayoutConstraint.activate([
            activityIndicator.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            activityIndicator.topAnchor.constraint(equalTo:  view.layoutMarginsGuide.topAnchor, constant: 10),
            activityIndicator.centerXAnchor.constraint(equalTo: view.layoutMarginsGuide.centerXAnchor)
        ])
        
        view.addSubview(schoolNameLabel, translatesAutoresizingMaskIntoConstraints: false)
        NSLayoutConstraint.activate([
            schoolNameLabel.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor, constant: 10),
            schoolNameLabel.topAnchor.constraint(equalTo:  view.layoutMarginsGuide.topAnchor, constant: 10),
            schoolNameLabel.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor)
        ])
        
        view.addSubview(satScoresTextView, translatesAutoresizingMaskIntoConstraints: false)
        NSLayoutConstraint.activate([
            satScoresTextView.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor, constant: 10),
            satScoresTextView.topAnchor.constraint(equalTo:  schoolNameLabel.bottomAnchor, constant: 10),
            satScoresTextView.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor)
        ])
        
        view.addSubview(mapView, translatesAutoresizingMaskIntoConstraints: false)
        NSLayoutConstraint.activate([
            mapView.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            mapView.topAnchor.constraint(equalTo:  satScoresTextView.bottomAnchor, constant: 10),
            mapView.trailingAnchor.constraint(equalTo:  view.layoutMarginsGuide.trailingAnchor),
            mapView.bottomAnchor.constraint(equalTo:  view.layoutMarginsGuide.bottomAnchor)
        ])
    }
    
    func updateViews() {
        schoolNameLabel.text = viewModel.schoolName
        satScoresTextView.text = viewModel.satScoresText
        mapView.centerToLocation(viewModel.clLocation, regionRadius: viewModel.clLocationDistance)
        let schoolAnnotation = MKPointAnnotation()
        schoolAnnotation.title = viewModel.schoolName
        schoolAnnotation.coordinate = CLLocationCoordinate2D(latitude: viewModel.latitude, longitude: viewModel.longitude)
        mapView.addAnnotation(schoolAnnotation)
        UIView.animate(
            withDuration: 1,
            animations: { [weak self] in
                guard let mapCamera = self?.viewModel.mapCamera else {
                    return
                }
                self?.mapView.camera = mapCamera
            })
    }
}

private extension MKMapView {
  func centerToLocation(
    _ location: CLLocation,
    regionRadius: CLLocationDistance
  ) {
    let coordinateRegion = MKCoordinateRegion(
      center: location.coordinate,
      latitudinalMeters: regionRadius,
      longitudinalMeters: regionRadius)
    setRegion(coordinateRegion, animated: true)
  }
}

